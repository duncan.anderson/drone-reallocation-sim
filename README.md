# Drone-Reallocation-Sim

Final project for Systems Simulation at UT Austin.

Explores dynamic capacity reallocation for food delivery drones and attempts to construct individual and joint confidence intervals around performance evaluations.